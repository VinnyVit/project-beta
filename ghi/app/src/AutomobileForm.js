import { useState, useEffect } from "react";

export default function AutomobileForm() {
    const [color, setColor] = useState('');
    const [year, setYear] = useState('')
    const [vin, setVin] = useState('')
    const [model, setModel] = useState('')

    const handleChangeVin = (event) => {
        const value = event.target.value;
        setVin(value);
      }


    const handleChangeYear = (event) => {
        const value = event.target.value;
        setYear(value);
      }


    const handleChangeColor = (event) => {
        const value = event.target.value;
        setColor(value);
      }

    const handleChangeModel = (event) => {
        const value = event.target.value;
        setModel(value);
      }

    const [models, setModels] = useState([]);
    const getModels = async function() {
      const url = 'http://localhost:8100/api/models/'
      const response = await fetch(url)
      if (response.ok) {
        const{models} = await response.json();
        setModels(models)
      }

    }

    useEffect(() => {
      getModels();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;

        const automobileUrl = `http://localhost:8100/api/automobiles/`;
        const fetchOptions = {
          method: 'post',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const automobileResponse = await fetch(automobileUrl, fetchOptions);


        if (automobileResponse.ok) {
          setColor('');
          setModel('');
          setVin('');
          setYear('');

        }
      }

return (

  <div className="container">
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1 style={{display: 'grid', justifyContent: 'center'}}>Create a new Automobile</h1>
          <form onSubmit={handleSubmit} id="create-presentation-form">
            <div className="form-floating mb-3">
              <input onChange={handleChangeColor} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeYear} value={year}placeholder="Year" required type="number" min={1900} max={2099} step={1} name="year" id="year" className="form-control"/>
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeVin} value={vin} placeholder="VIN" type="text" name="vin" id="vin" className="form-control"/>
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="mb-3">
              <select onChange={handleChangeModel} value={model}required name="model" id="model" className="form-select">
                <option>Model</option>
                {models?.map(model => {
                      return (
                        <option key={model.id} value={model.id}>{model.name}</option>
                      )
                    })}
              </select>
            </div>
            <div style={{display: 'grid', justifyContent: 'center'}}>
              <button className="btn btn-primary" >Create</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  );
}
