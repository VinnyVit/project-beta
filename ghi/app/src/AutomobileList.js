import { useEffect, useState } from "react";


function AutomobileList(){
    const [automobiles, setAutos] = useState([]);
    const getAutos = async function() {
      const url = 'http://localhost:8100/api/automobiles/'
      const response = await fetch(url)
      if (response.ok) {
        const automobiles = await response.json();
        setAutos(automobiles.autos)
      }
    }

    useEffect(() => {
      getAutos();
    }, []);
    return (
      <>
        <br></br><h1>Automobiles</h1><br></br>
        <table className="table table-striped">
            <thead>
              <tr>
                <th>VIN</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturer</th>
                <th>Sold</th>
              </tr>
            </thead>
            <tbody>
              {automobiles?.map(auto => {
                return (
                  <tr key={auto.href}>
                    <td>{ auto.vin }</td>
                    <td>{auto.color}</td>
                    <td>{auto.year}</td>
                    <td>{auto.model.name}</td>
                    <td>{auto.model.manufacturer.name}</td>
                    {auto.sold === false &&
                      <td>
                        No
                      </td>
                    }
                    {auto.sold === true &&
                      <td>
                        Yes
                      </td>
                    }
                    {/* <td>{auto.sold}</td> */}
                  </tr>
                );
            })}
            </tbody>
        </table>
      </>
    );
}
export default AutomobileList;
