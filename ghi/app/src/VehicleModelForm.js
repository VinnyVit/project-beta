import React, { useEffect, useState } from "react";


function VehicleModelForm() {
    const [manufacturers, setManufacturers] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const [formData, setFormData] = useState({
        name: "",
        pictureUrl: "",
        manufacturer: "",
    });

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = formData.name;
        data.picture_url = formData.pictureUrl;
        data.manufacturer_id = formData.manufacturer;



        const modelUrl = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            await response.json();


            setFormData({
                name: "",
                pictureUrl: "",
                manufacturer: "",
            });
        };
    };

    const handleChange= (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value,
        })
    };

    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1 style={{display: 'grid', justifyContent: 'center'}}>Create a new vehicle model</h1>
                        <form onSubmit={handleSubmit} id="create-model-form" >
                            <div className="form-floating mb-3">
                                <input onChange={handleChange} value={formData.name} placeholder="Model Name" required type="text" name="name" id="name" className="form-control"/>
                                <label htmlFor="name">Model Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleChange} value={formData.pictureUrl} placeholder="Picture Url" required type="url" name="pictureUrl" id="pictureUrl" className="form-control"/>
                                <label htmlFor="pictureUrl">Picture Url</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleChange} value={formData.manufacturer} required id="manufacturer" name="manufacturer" className="form-select">
                                    <option value="">Choose a Manufacturer</option>
                                    {manufacturers?.map(manufacturer => {
                                        return (
                                            <option value={manufacturer.id} key={manufacturer.id}>
                                                {manufacturer.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div style={{display: 'grid', justifyContent: 'center'}}>
                                <button className="btn btn-primary">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}

export default VehicleModelForm;
