import { useEffect, useState } from "react";


function VehicleModelList() {
    const [models, setModels] = useState([]);
    const getModels = async function() {
      const url = 'http://localhost:8100/api/models/'
      const response = await fetch(url)
      if (response.ok) {
        const{models} = await response.json();
        setModels(models)
      }
    }

    useEffect(() => {
      getModels();
    }, []);

    return (
      <>
        <br></br><h1>Vehicle Models</h1><br></br>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Manufacturer</th>
              <th>Picture</th>
            </tr>
          </thead>
          <tbody>
            {models?.map(model => {
              return (
                <tr key={model.href}>
                  <td>{ model.name }</td>
                  <td>{model.manufacturer.name}</td>
                  <td><img src= {model.picture_url} height={100} border={3} width={300} alt=""></img></td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </>
    );
}

export default VehicleModelList;
