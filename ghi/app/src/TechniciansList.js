import React, { useEffect, useState } from "react";


function TechniciansList() {
    const [technicians, setTechnicians] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <br></br><h1>Technicians</h1><br></br>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Employee Id</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                </tr>
              </thead>
              <tbody>
                {technicians?.map(technician => {
                  return (
                    <tr key={technician.id}>
                      <td>{ technician.employee_id }</td>
                      <td>{ technician.first_name }</td>
                      <td>{ technician.last_name }</td>
                    </tr>
                  );
                })}
              </tbody>
          </table>
        </>
      )
}

export default TechniciansList
