import React, { useEffect, useState } from "react";


function SalesList() {
    const [sales, setSales] = useState([]);

    const getSales = async function() {
      const url = 'http://localhost:8090/api/sales/'
      const response = await fetch(url)
      if (response.ok) {
        const sales = await response.json();
        setSales(sales)
      }
    }

    useEffect(() => {
        getSales();
    }, []);

    return (
        <>
            <br></br><h1>Sales History</h1><br></br>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Salesperson Id</th>
                  <th>Salesperson Name</th>
                  <th>Customer</th>
                  <th>VIN</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                {sales?.map(sale => {
                  return (
                    <tr key={sale.id}>
                      <td>{ sale.salesperson.employee_id}</td>
                      <td>{ sale.salesperson.first_name } {sale.salesperson.last_name}</td>
                      <td>{ sale.customer.first_name} {sale.customer.last_name}</td>
                      <td>{sale.automobile.vin}</td>
                      <td>${sale.price}</td>
                    </tr>
                  );
                })}
              </tbody>
          </table>
        </>
      )
}

export default SalesList
