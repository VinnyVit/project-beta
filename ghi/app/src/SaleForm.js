import React, { useState, useEffect } from "react";


function SaleForm() {
    const [formData, setFormData] = useState({
        price: "",
        automobile: "",
        salesperson: "",
        customer: "",
    });

    const handleChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value,
        })
    };
    const [customers, setCustomers] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);
        if (response.ok) {
          const customers = await response.json();
          setCustomers(customers);
        }
      }

      useEffect(() => {
        fetchData();
      }, []);

      const [automobiles, setAutos] = useState([]);
      const getAutos = async function() {
        const url = 'http://localhost:8090/automobilevo/'
        const response = await fetch(url)

        if (response.ok) {
          const automobilesList = await response.json();
          const automobile = automobilesList.autos
          const unsoldAutos = automobile.filter(autos => autos.sold === false)
          setAutos(unsoldAutos)
        }
      }

      useEffect(() => {
        getAutos();
      }, []);

      const [salespeople, setSalespeople] = useState([]);

      const getSalespeople = async function() {
        const url = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(url)

        if (response.ok) {
          const salespeople = await response.json();
          setSalespeople(salespeople)
        }
      }

      useEffect(() => {
          getSalespeople();
      }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();

        const salesUrl = "http://localhost:8090/api/sales/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                price: "",
                customer: "",
                salesperson: "",
                automobile: ""
            });
        };
    };

    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1 style={{display: 'grid', justifyContent: 'center'}}>Record a Sale</h1>
                        <form onSubmit={handleSubmit} id="add-technician-form" >
                            <div className="form-floating mb-3">
                                <select onChange={handleChange} required name="automobile" id="automobile" className="form-select" value={formData.automobile}>
                                    <option value="">Choose an Automobile VIN</option>
                                    {automobiles?.map(auto => {
                                        return (
                                            <option key={auto.vin} value={auto.vin}>{auto.vin}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <select onChange={handleChange} required name="customer" id="customer" className="form-select" value={formData.customer}>
                                    <option value="">Choose a Customer</option>
                                    {customers?.map(customer => {
                                        return (
                                            <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                                        )
                                        })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <select onChange={handleChange} required name="salesperson" id="salesperson" className="form-select" value={formData.salesperson}>
                                    <option value="">Choose a Salesperson</option>
                                    {salespeople?.map(salesperson => {
                                        return (
                                            <option key={salesperson.employee_id} value={salesperson.employee_id}>{salesperson.first_name} {salesperson.last_name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleChange} value={formData.price} placeholder="price" required type="number" step={0.01} min={0} max={10000} name="price" id="price" className="form-control"/>
                                <label htmlFor="price">Price</label>
                            </div>
                            <div style={{display: 'grid', justifyContent: 'center'}}>
                                <button className="btn btn-primary">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}

export default SaleForm
