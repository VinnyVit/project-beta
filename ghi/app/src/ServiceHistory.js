import React, { useEffect, useState } from "react";


function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [vin, setVin] = useState('');
    const [searchTerm, setSearchTerm] = useState('');

    const fetchData = async () => {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        const automobileUrl = "http://localhost:8080/api/automobilevos/";
        const voResponse = await fetch(automobileUrl);
        if (response.ok && voResponse.ok) {
            const data = await response.json();
            const vo = await voResponse.json();
            const appointmentList = data.appointments;
            const voList = vo.automobilevos;
            const vinList = voList.map(vo => vo.vin)
            // add VIP info and format date_time object
            for (let appointment of appointmentList) {
                if (vinList.includes(appointment.vin)) {
                    appointment["vip"] = "Yes";
                } else {
                    appointment["vip"] = "No";
                }
            }
            setAppointments(appointmentList);
        };
    };
    useEffect(() => {fetchData()},[])

    return (
        <>
            <br></br><h1>Service History</h1><br></br>
            <div style={{display: "flex"}} className="form-floating mb-3">
                <input value={vin} onChange={({target}) => setVin(target.value)} placeholder="Automobile VIN" required type="text" name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">Automobile VIN</label>
                <button className="btn btn-info" onClick={() => setSearchTerm(vin)}>Search</button>
            </div>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>VIN</th>
                  <th>Is VIP?</th>
                  <th>Customer</th>
                  <th>Date</th>
                  <th>Time</th>
                  <th>Technician</th>
                  <th>Reason</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                {appointments?.filter(appointment => searchTerm ? appointment.vin === searchTerm : true).map(appointment => {
                  const {id, vin, vip, customer, date_time, technician, reason , status} = appointment;
                  const date = new Date(date_time);
                  const timeFormat = {
                    hour: 'numeric',
                    minute: 'numeric',
                    second: 'numeric',
                    hour12: true,
                  }
                  return (
                    <tr key={id}>
                      <td>{ vin }</td>
                      <td>{ vip }</td>
                      <td>{ customer }</td>
                      <td>{ date.toLocaleDateString('en-US') }</td>
                      <td>{ date.toLocaleTimeString('en-US', timeFormat)}</td>
                      <td>{ technician.first_name } { technician.last_name }</td>
                      <td>{ reason }</td>
                      <td>{ status }</td>
                    </tr>
                  );
                })}
              </tbody>
          </table>
        </>
      );
};
export default ServiceHistory;
