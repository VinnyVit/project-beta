from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class AutomobileVo(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Salesperson(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.IntegerField(primary_key=True)


class Customer(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    address = models.CharField(max_length=200)
    phone_number = models.IntegerField(validators=[MaxValueValidator(9999999999), MinValueValidator(100000000)],default=1000000001, unique=True)


class Sale(models.Model):
    price = models.DecimalField(max_digits=7, decimal_places=2)
    automobile = models.ForeignKey(AutomobileVo, related_name="sale", on_delete=models.CASCADE, default='')
    salesperson = models.ForeignKey(Salesperson, related_name="sale", on_delete=models.CASCADE, default='')
    customer = models.ForeignKey(Customer, related_name="sale", on_delete=models.CASCADE, default='')
