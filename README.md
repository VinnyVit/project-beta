# CarCar 🚗
![Project Screenshot](service/api/common/images/sample.png)
<br><br>
Team:

* Vincent Vitiritto - Auto Sales
* Grace Lee - Auto Services
* Everyone - Inventory

## Set Up
1. Fork repository from [this repository](https://gitlab.com/gracehlee/project-beta).
2. Open terminal and navigate to desired directory.
3. Run the command:<br>
<code>git clone https://gitlab.com/gracehlee/project-beta.git</code>
4. Navigate to repository folder:<br>
<code>cd project-beta</code>
5. Open in VS Code:<br>
<code>code .</code>
6. Open the Docker application.
7. In the VS Code Terminal, run the commands:<br>
<code>docker volume create beta-data</code><br>
<code>docker-compose build</code><br>
<code>docker-compose up</code>
8. After containers are running, open in browser:<br>
http://localhost:3000/

## Design
![Project Diagram](service/api/common/images/CarCar.png)
- Bounded Contexts are indicated by the colored background circles.
- Value Objects are indicated by the abbreviation "VO".


## Inventory microservice
#### Manufacturer 🚗
---

- **Create - POST: http://localhost:8100/api/manufacturers/**

Required JSON Body Format:

    {
        "name": "<<your manufacturer name here>>"
    }

Example Output:

    {
        "href": "/api/manufacturers/:id/",
        "id": 1,
        "name": "your manufacturer's name"
    }

<br>

- **Read - GET: http://localhost:8100/api/manufacturers/**

Example Output:

    {
        "manufacturers": [
            {
                "href": "/api/manufacturers/3/",
                "id": 3,
                "name": "manufacturer 3"
            },
            {
                "href": "/api/manufacturers/4/",
                "id": 4,
                "name": "manufacturer 4"
            }
        ]
    }

<br>

- **Update - PUT: http://localhost:8100/api/manufacturers/<int:id>/**

Required JSON Body Format:

    {
        "name": "<<updated name here>>"
    }

- **Delete - DELETE: http://localhost:8100/api/manufacturers/<int:id>/**

Example Output:

    {
        "id": null,
        "name": "<<name of deleted manufacturer>>"
    }

Example Failed Output:

    {
        "message": "Does not exist"
    }

#### Automobile 🚗
---

- **Create - POST: http://localhost:8100/api/automobiles/**

Required JSON Body Format:

    {
        "color": "red",
        "year": 2024,
        "vin": "<<unique vin>>",
        "model_id": <<id of existing vehicle model id>>
    }

Example Output:

    {
        "href": "/api/automobiles/1C3CC5FB2AN120124/",
        "id": 1,
        "color": "red",
        "year": 2024,
        "vin": "1C3CC5FB2AN120124",
        "model": {
            "href": "/api/models/1/",
            "id": 1,
            "name": "Sebring",
            "picture_url": "https://exampleurl.org/example.png",
            "manufacturer": {
                "href": "/api/manufacturers/1/",
                "id": 1,
                "name": "Manufacturer Name"
            }
        },
        "sold": false
    }

<br>

- **Read - GET: http://localhost:8100/api/automobiles/**

Example Output:

    {
        "autos": [
            {
                "href": "/api/automobiles/1C3CC5FB2AN120174/",
                "id": 5,
                "color": "red",
                "year": 2012,
                "vin": "1C3CC5FB2AN120174",
                "model": {
                    "href": "/api/models/4/",
                    "id": 4,
                    "name": "name",
                    "picture_url": "url.url",
                    "manufacturer": {
                        "href": "/api/manufacturers/3/",
                        "id": 3,
                        "name": "manufacturer 3"
                    }
                },
                "sold": false
            }
        ]
    }


<br> 

- **Update - PUT: http://localhost:8100/api/automobiles/:vin/**

Required JSON Body Format:

    {
        "color": "red",
        "year": 2012,
        "vin": "<<unique vin>>",
        "model_id": <<existing vehicle model id>>
    }

<br>

- **Delete - DELETE: http://localhost:8100/api/automobiles/:vin/**

Example Output:

    {
        "href": "/api/automobiles/1233223/",
        "id": null,
        "color": "test2",
        "year": 1900,
        "vin": "1233223",
        "model": {
            "href": "/api/models/<int:id>/",
            "id": 5,
            "name": "name",
            "picture_url": "url",
            "manufacturer": {
                "href": "/api/manufacturers/<int:id>/",
                "id": 3,
                "name": "name"
            }
        },
        "sold": false
    }

Example Failed Output:

    {
        "message": "Does not exist"
    }

<br>

#### Vehicle Model 🚗
---

- **Create - POST: http://localhost:8100/api/models/**

Required JSON Body Format:

    {
        "name": "example",
        "picture_url": "https://exampleurl.org/example.png",
        "manufacturer_id": <<existing manufacturer id>>
    }

Example Output:

    {
        "href": "/api/models/1/",
        "id": 1,
        "name": "example",
        "picture_url": "https://exampleurl.org/example.png",
        "manufacturer": {
            "href": "/api/manufacturers/1/",
            "id": 1,
            "name": "name"
        }
    }

<br>

- **Read - GET: http://localhost:8100/api/models/**

Example Output:

    {
        "models": [
            {
                "href": "/api/models/4/",
                "id": 4,
                "name": "name",
                "picture_url": "url.url",
                "manufacturer": {
                    "href": "/api/manufacturers/3/",
                    "id": 3,
                    "name": "manufacturer 4"
                }
            }
        ]
    }

- **Update - PUT: http://localhost:8100/api/models/<int:id>/**

Required JSON Body Format:

    {
        "name": "example",
        "picture_url": "https://exampleurl.org/example.png",
        "manufacturer_id": <<existing manufacturer id>>
    }

- **Delete - DELETE: http://localhost:8100/api/models/<int:id>/**

Example Output:

    {
        "id": null,
        "name": "name",
        "picture_url": "url",
        "manufacturer": {
            "href": "/api/manufacturers/<int:id>/",
            "id": 1,
            "name": "name"
        }
    }

Example Failed Output:

    {
        "message": "Does not exist"
    }

## Service microservice

Service microservice port: 8080

The Service microservice consists of 3 models:
1. Technician
2. Appointment
3. AutomobileVO

In service/poll/poller.py, we access the Inventory microservice model "Automobile" through a "GET" request, and integrate the data into the Service microservice model AutomobileVO. Thus, the "GET" request is sent to the endpoint:<br>
<code>http://inventory-api-1:8000/api/automobiles/</code>

---

The following documentation includes the CRUD Routes for each of the Service models; note that each model may not have all elements of the CRUD elements depending on the project's design and needs.

---

### Technician 🚗
---

- **Create - POST: http://localhost:8080/api/technicians/**
<br>Create a Technician

Required JSON Body Format:

    {
        "first_name": "name",
        "last_name": "name",
        "employee_id": "<<id>>"
    }

Expected Output:

    {
        "first_name": "name",
        "last_name": "name",
        "employee_id": "<<id>>",
        "id": 1
    }
<br>

- **Read - GET: http://localhost:8080/api/technicians/**
<br>List Technicians

Expected Output:

    {
        "technicians": [
            {
                "first_name": "name",
                "last_name": "name",
                "employee_id": "<<id>>",
                "id": 1
            },
            {
                "first_name": "bob",
                "last_name": "bob",
                "employee_id": "123",
                "id": 2
            },
            ...
        ]
    }

Expected Output if empty:
<br>


    {
        "technicians": []
    }
<br>

- **Delete - DELETE: http://localhost:8080/api/technicians/<int:id>/**
<br>Delete a specific Technician

Expected Output:

    {
        "deleted": true
    }

Failed Output:

    {
        "deleted": false
    }

---

The Technician model will be used in our Technician list and form, and will be referenced by the Appointment model as a ForeignKey relation.

---

### Appointment 🚗
---

- **Create - POST: http://localhost:8080/api/appointments/**
<br>Create an Appointment

Required JSON Body Format:

    {
        "reason": "reason",
        "vin": "any vin id",
        "customer": "name",
        "technician": <<existing technician id>>
    }

Expected Output:

    {
        "date_time": "YYYY-MM-DDTHH:MM:SS.566651+00:00",
        "reason": "reason",
        "status": "created",
        "vin": "any vin id",
        "customer": "name",
        "technician": {
            "first_name": "name",
            "last_name": "name",
            "employee_id": "employee id",
            "id": <<technician id>>
        },
        "id": 1
    }


- **Read - GET: http://localhost:8080/api/appointments/**
<br>List Appointments

Expected Output:

    {
        "appointments": [
            {
                "date_time": "YYYY-MM-DDTHH:MM:SS.566651+00:00",
                "reason": "reason",
                "status": "created",
                "vin": "any vin id",
                "customer": "name",
                "technician": {
                    "first_name": "name",
                    "last_name": "name",
                    "employee_id": "employee id",
                    "id": <<technician id>>
                },
                "id": 1
            },
            {
                "date_time": "2024-03-19T20:55:09.374491+00:00",
                "reason": "hi",
                "status": "finished",
                "vin": "123",
                "customer": "bob bob",
                "technician": {
                    "first_name": "bob",
                    "last_name": "bob",
                    "employee_id": "123",
                    "id": 1
                },
                "id": 2
            }
        ]
    }

Expected Output if empty:

    {
        "appointments": []
    }

<br>

- **Update - Cancel PUT: [`http://localhost:8080/api/appointments/<int:id>/cancel/`](http://localhost:8080/api/appointments/<int:id>/cancel/)**
<br>Set Appointment status to "canceled"

No Required JSON Body.

Expected Output:

    {
        ...
        "status": "canceled",
        ...
    }



- **Update - Finish PUT: [`http://localhost:8080/api/appointments/<int:id>/finish/`](http://localhost:8080/api/appointments/<int:id>/finish/)**
<br>Set Appointment status to "finished"

No Required JSON Body.

Expected Output:

    {
        ...
        "status": "finished",
        ...
    }

- **Delete - DELETE: http://localhost:8080/api/appointments/<int:id>/**
<br>Delete an Appointment

Expected Output:

    {
        "deleted": true
    }

Failed Output:

    {
        "deleted": false
    }

---

The Appointment model will be used in our Appointment List, Form, and Service History pages. It references the Technician model as a ForeignKey relation. On the front-end, the Appointment List will reference the VIN data from the AutomobileVO to determine VIP status. If the VIN provided for an appointment exists in our inventory, meaning it was sold, the Appointment List will display "Yes" for the VIP status. Using the "Update" routes, the Appointment can also update the appointment instance's status to 'canceled' or 'finished', which will remove it from the Appointment List view. This does not delete the instance of the appointment, as it will still be accessible from the Service History view.

---

### AutomobileVO 🚗
---

- **Read - GET: http://localhost:8080/api/automobilevos/**
<br>List AutomobileVOs

Expected Output:

    {
        "automobilevos": [
            {
                "vin": "1C3CC5FB2AN120174",
                "sold": false
            },
            {
                "vin": "sdf",
                "sold": false
            }
        ]
    }

Expected Output if empty:

    {
        "automobilevos": []
    }

---

Since AutomobileVO is a value object that is obtained from the Inventory microservice model "Automobile", the only CRUD route implemented is the "READ" route. Otherwise, the Automobile model from the Inventory microservice should be accessed to create any direct changes to the Automobiles (and in turn, the AutomobileVOs will update accordingly through poller.py).
<br><br>
The VIN information from the AutomobileVO will be utilized in the Appointment List to determine VIP status of a customer.

---

## Sales microservice

The sales microservice consists of four models, and their subsequent view functions. The Salesperson model relates to a salesperson to be associated with transactions. The customer model relates to a customer that a transaction would be associated with. AutomobileVo is a value object that grabs its data using a poller found in sales/poll/poller. In order to attach an automobile to a sale, we need that automobile's vin number. To do so, the poller polls the Inventory microservice to store that data. The Sale model acts as a record for a sale, it contains three foreign keys to the other models, and its own price field to record a transaction.

## Accessing Sales Microservice Endpoints

|Function| Method| Endpoint |
| ----------- | ----------- |-----------|
| List Salespeople | GET | http://localhost:8090/api/salespeople/ |
| Create a Salesperson | POST| http://localhost:8090/api/salespeople/ |
| Delete a Salesperson | DELETE | http://localhost:8090/api/salespeople/:id/ |
| List Customers | GET | http://localhost:8090/api/customers/ |
| Create a Customer | POST | http://localhost:8090/api/customers/ |
| Delete a Customer | DELETE | http://localhost:8090/api/customers/:id/ |
| List Sales | GET |http://localhost:8090/api/sales/ |
| Create a Sale | POST| http://localhost:8090/api/sales/ |
| Delete a Sale | DELETE | http://localhost:8090/api/sales/:id/ |


## Data Formatting

## Salespeople

To create a Salesperson, the body of your data must be sent as JSON like so:

    {
        "first_name": "Jim",
        "last_name": "Smith",
        "employee_id": 1
    }


employee ids must be unique

Example Output:

    {
        "first_name": "Jim",
        "last_name": "Smith",
        "employee_id": 1
    }



## Customers
To create a Customer, the body of your data must be sent as JSON like so:

    {
        "first_name": "John",
        "last_name": "Berch",
        "address": "123 North 10th Street",
        "phone_number": 1234567891

    }

phone numbers must be unique

Example Output:

    {
        "first_name": "John",
        "last_name": "Berch",
        "address": "123 North 10th Street",
        "phone_number": 1234567891,
        "id": 1
    }

Failed Output:

    {
        "message": "Could not create customer, ensure phone number is correct and unique"
    }



## Sales
To record a sale, the body of your data must be sent as JSON like so:

    {
        "price":1000,
        "automobile": "1C3CC5FB2AN120174",
        "salesperson": 1,
        "customer": 1
    }

salesperson must match an employee's id.
customer must match a customer's id.
automobile represents the VIN number tied to an AutomobileVo object, this must match a previously created automobile's VIN.

Example Output:

    {
        "id": 1,
        "price": 1000,
        "automobile": {
            "vin": "1C3CC5FB2AN120174",
            "sold": true
        },
        "customer": {
            "first_name": "first name",
            "last_name": "last name",
            "address": "123 address avenue",
            "phone_number": "1234567891",
            "id": 1
        },
        "salesperson": {
            "first_name": "first name",
            "last_name": "last name",
            "employee_id": 1
        }
    }

Failed Output:

    {
        "message": "Invalid Automobile"
    }

Occurs when a VIN number does not match an existing automobile


    {
        "message": "Invalid Customer"
    }

Occurs when a customer id does not match an existing customer

    {
        "message": "Invalid Salesperson"
    }

Occurs when a salesperson id does not match an existing salesperson
